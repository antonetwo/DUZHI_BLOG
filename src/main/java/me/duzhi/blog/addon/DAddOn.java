package me.duzhi.blog.addon;

import io.jpress.core.addon.AddonInfo;

/**
 * @author ashang.peng@aliyun.com
 * @date 二月 18, 2017
 */

public  class DAddOn {
    private AddonInfo addon;
    private String html;

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public AddonInfo getAddon() {
        return addon;
    }

    public void setAddon(AddonInfo addon) {
        this.addon = addon;
    }

}
